# mish-vtb-tender

Верстка информационного раздела для продукта "Дебетовая Мультикарта ВТБ".

Реализовано на [Create React App](https://github.com/facebook/create-react-app).

* Дизайн - https://mish.design/
* Макеты - https://www.figma.com/file/nlYaOFkGH1lwFGaxfck6hG/%D0%92%D0%A2%D0%91-%D0%9A%D0%BE%D0%BD%D1%86%D0%B5%D0%BF%D1%82?node-id=0%3A1

* Репозиторий - https://gitlab.com/karambafe/mish-vtb-tender
* Сайт - https://mish-vtb-tender.now.sh/

## Запуск в режиме разработки

1. `npm ci`
1. `npm start`

## Продакшен сборка

1. `npm ci`
1. `npm run build`

## Запуск продакшен сборки

Может потребоваться глобальная установка сервера статики

1. `npm i -g serve`
1. `serve -s build`
