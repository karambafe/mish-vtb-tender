/* eslint-disable jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Checkbox from 'custom-elements/Checkbox/Checkbox';

import './Radio.scss';

const Radio = ({
  title,
  image,
  name,
  value,
  checked,
  onChange,
}) => (
  <div className="radio">
    <div className="radio__checkbox">
      <Checkbox
        id={value}
        name={name}
        checked={checked}
        value={value}
        onChange={onChange}
      />
    </div>

    <div
      className={classNames('radio__content', {
        'radio__content_has-title': title,
      })}
      onClick={() => onChange(value)}
    >
      <span className="radio__title">{title}</span>

      <img
        src={image.src}
        alt={value}
        width={image.width}
        height={image.height}
        className="radio__image"
      />
    </div>
  </div>
);

Radio.propTypes = {
  title: PropTypes.string,
  image: PropTypes.shape({
    src: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
  }).isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
};

Radio.defaultProps = {
  title: '',
  checked: false,
  onChange: () => {},
};

export default Radio;
