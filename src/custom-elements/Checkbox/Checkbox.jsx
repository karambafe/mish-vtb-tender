import React from 'react';
import PropTypes from 'prop-types';

import { ReactComponent as CheckIcon } from 'assets/images/check.svg';

import './Checkbox.scss';

const Checkbox = ({
  name,
  value,
  checked,
  disabled,
  onChange,
}) => (
  <label htmlFor={value} className="checkbox">
    <input
      id={value}
      className="checkbox__input"
      type="checkbox"
      name={name}
      checked={checked}
      disabled={disabled}
      value={value}
      onChange={({ target }) => onChange(target.value)}
    />

    <span className="checkbox__icon">
      <CheckIcon />
    </span>
  </label>
);

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
};

Checkbox.defaultProps = {
  checked: false,
  disabled: false,
  onChange: () => {},
};

export default Checkbox;
