import React from 'react';
import PropTypes from 'prop-types';
import Swiper from 'react-id-swiper';
import classNames from 'classnames';

import 'swiper/swiper.scss';
import './Tabs.scss';

const DEFAULT_RULES = {
  320: {
    spaceBetween: 32,
  },
};

const Tabs = ({
  items,
  activeTab,
  extendedTabClass,
  swiperBreakpointsRules,
  onClick,
}) => (
  <Swiper
    WrapperEl="nav"
    wrapperClass="tabs"
    slidesPerView="auto"
    freeMode
    watchOverflow
    breakpoints={swiperBreakpointsRules || DEFAULT_RULES}
  >
    {items.map(item => (
      <button
        key={item.value}
        type="button"
        className={classNames('tabs__item', {
          tabs__item_active: activeTab === item.value,
          [extendedTabClass]: true,
        })}
        disabled={item.disabled}
        onClick={() => onClick(item.value)}
      >
        {item.label}
      </button>
    ))}
  </Swiper>
);

Tabs.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    disabled: PropTypes.bool,
  })).isRequired,
  activeTab: PropTypes.string.isRequired,
  extendedTabClass: PropTypes.string,
  swiperBreakpointsRules: PropTypes.objectOf(PropTypes.object),
  onClick: PropTypes.func,
};

Tabs.defaultProps = {
  extendedTabClass: '',
  swiperBreakpointsRules: undefined,
  onClick: () => {},
};

export default Tabs;
