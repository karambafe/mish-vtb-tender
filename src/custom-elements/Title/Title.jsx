import React from 'react';
import PropTypes from 'prop-types';

import './Title.scss';

const Title = ({
  title,
  level,
  weight,
  color,
}) => {
  const CustomTag = `h${level}`;

  return (
    <CustomTag className={`title title_color_${color} title_level_${level} title_weight_${weight}`}>
      { title }
    </CustomTag>
  );
};

Title.propTypes = {
  title: PropTypes.string.isRequired,
  level: PropTypes.PropTypes.oneOf([1, 2, 3, 4]),
  weight: PropTypes.PropTypes.oneOf(['thin', 'normal', 'bold']),
  color: PropTypes.PropTypes.oneOf(['black', 'white']),
};

Title.defaultProps = {
  level: 2,
  weight: 'thin',
  color: 'black',
};

export default Title;
