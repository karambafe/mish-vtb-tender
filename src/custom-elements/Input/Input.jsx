/* eslint-disable react/destructuring-assignment, react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { IMaskInput } from 'react-imask';
import { Tooltip } from 'react-tippy';

import { ReactComponent as InfoIcon } from 'assets/images/info.svg';

import './Input.scss';

// TODO thousandsSeparator задавать в геттере mask
class Input extends React.PureComponent {
  get isEmpty() {
    return this.props.value.length === 0;
  }

  get hasCaption() {
    return this.props.caption.length > 0;
  }

  get hasError() {
    return this.props.error.length > 0;
  }

  get hasInfo() {
    return this.props.info.length > 0;
  }

  get mask() {
    if (this.props.maskType === 'phone') return { mask: '+{7} (000) 000-00-00' };
    if (this.props.maskType === 'date') {
      return {
        mask: Date,
        min: new Date(1990, 0, 1),
        max: new Date(2020, 0, 1),
      };
    }
    if (this.props.maskType === 'number') {
      return {
        mask: Number,
        min: 6,
        max: 500000,
        thousandsSeparator: ' ',
      };
    }
    return '';
  }

  handleChange = ({ target: { value } }) => {
    this.props.onChange({ value, id: this.props.id });
  }

  handleAccept = value => {
    this.props.onChange({ value, id: this.props.id });
  };

  render() {
    return (
      <div className="input-field">
        <div className="input-field__container">
          {this.props.maskType ? (
            <IMaskInput
              id={this.props.id}
              type={this.props.type}
              name={this.props.id}
              className={classNames('input-field__input', {
                'input-field__input_has-caption': this.hasCaption,
                'input-field__input_has-error': this.hasError,
              })}
              data-empty={this.isEmpty.toString()}
              autoComplete={this.props.autocomplete}
              required={this.props.required}
              disabled={this.props.disabled}
              value={this.props.value}
              unmask
              inputRef={el => { this.input = el; }}
              onAccept={this.handleAccept}
              {...this.mask}
            />
          ) : (
            <input
              id={this.props.id}
              type={this.props.type}
              name={this.props.id}
              className={classNames('input-field__input', {
                'input-field__input_has-caption': this.hasCaption,
                'input-field__input_has-error': this.hasError,
                'input-field__input_has-info': this.hasInfo,
              })}
              data-empty={this.isEmpty.toString()}
              autoComplete={this.props.autocomplete}
              required={this.props.required}
              disabled={this.props.disabled}
              value={this.props.value}
              onChange={this.handleChange}
            />
          )}

          <label className="input-field__label" htmlFor={this.props.id}>{this.props.label}</label>

          <span className="input-field__caption">{this.props.caption}</span>

          {this.hasInfo && (
            <Tooltip
              animation="fade"
              animateFill={false}
              title={this.props.info}
              position="top-end"
              className="input-field__info"
            >
              <InfoIcon />
            </Tooltip>
          )}
        </div>

        <div
          className={classNames('input-field__error-container', {
            'input-field__error-container_show': this.hasError,
          })}
        >
          {this.props.error && <p className="input-field__error">{this.props.error}</p>}
        </div>
      </div>
    );
  }
}

Input.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'tel', 'email', 'number']),
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  caption: PropTypes.string,
  info: PropTypes.string,
  autocomplete: PropTypes.string,
  maskType: PropTypes.oneOf(['', 'number', 'phone', 'date']),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

Input.defaultProps = {
  type: 'text',
  error: '',
  caption: '',
  info: '',
  maskType: '',
  autocomplete: '',
  disabled: false,
  required: false,
};

export default Input;
