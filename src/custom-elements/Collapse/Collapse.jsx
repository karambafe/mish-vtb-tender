import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { ReactComponent as ArrowDownIcon } from 'assets/images/arrow-down.svg';

import './Collapse.scss';

const Collapse = ({
  label,
  children,
}) => {
  const [isToggled, setToggled] = useState(false);

  return (
    <div className="collapse">
      <button
        type="button"
        className="collapse__button"
        onClick={() => setToggled(!isToggled)}
      >
        {label}

        <span className={`collapse__icon ${isToggled ? 'collapse__icon_rotate' : ''}`}>
          <ArrowDownIcon />
        </span>
      </button>

      <div className="collapse__content-wrapper">
        <div className={`collapse__content-inner ${isToggled ? 'collapse__content-inner_open' : ''}`}>
          {children}
        </div>
      </div>
    </div>
  );
};

Collapse.propTypes = {
  label: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ]).isRequired,
};

export default Collapse;
