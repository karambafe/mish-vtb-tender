import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = ({
  label,
  type,
  color,
  disabled,
  onClick,
}) => (
  // eslint-disable-next-line react/button-has-type
  <button
    type={type}
    className={`button button_color_${color}`}
    disabled={disabled}
    onClick={onClick}
  >
    {label}
  </button>
);

Button.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['button', 'submit']),
  color: PropTypes.oneOf(['red', 'blue']),
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  type: 'button',
  color: 'blue',
  disabled: false,
  onClick: () => {},
};

export default Button;
