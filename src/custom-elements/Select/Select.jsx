import React from 'react';
import PropTypes from 'prop-types';

import { ReactComponent as ArrowDownIcon } from 'assets/images/arrow-down.svg';

import './Select.scss';

const Select = ({
  id,
  label,
  activeOption,
  options,
  onChange,
}) => {
  const activeOptionTitle = options.find(option => option.value === activeOption)?.title || '';

  return (
    <div className="select">
      <select id={id} className="select__select" onChange={({ target }) => onChange(target.value)}>
        {options.map(option => (
          <option
            key={option.value}
            className="select__option"
            value={option.value}
          >
            {option.title}
          </option>
        ))}
      </select>

      <label className="select__label" htmlFor={id}>
        <span className="select__label__content">
          <span className="select__label-text">{label}</span>

          <span className="select__active-item">{activeOptionTitle}</span>
        </span>

        <span className="select__icon">
          <ArrowDownIcon />
        </span>
      </label>
    </div>
  );
};

Select.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  activeOption: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    title: PropTypes.string,
  })).isRequired,
  onChange: PropTypes.func,
};

Select.defaultProps = {
  onChange: () => {},
};

export default Select;
