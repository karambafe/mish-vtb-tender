import React from 'react';
import PropTypes from 'prop-types';

import { ReactComponent as ArrowForwardIcon } from 'assets/images/arrow-forward.svg';

import './FormSubmitButton.scss';

const FormSubmitButton = ({ disabled }) => (
  <button
    type="submit"
    className="form-submit-button"
    disabled={disabled}
  >
    Следующий шаг
    <ArrowForwardIcon />
  </button>
);

FormSubmitButton.propTypes = {
  disabled: PropTypes.bool,
};

FormSubmitButton.defaultProps = {
  disabled: false,
};

export default FormSubmitButton;
