import React, { useState } from 'react';
import PropTypes from 'prop-types';

import BonusesSummarise from 'components/BonusesSummarised/BonusesSummarise';

import Title from 'custom-elements/Title/Title';
import Tabs from 'custom-elements/Tabs/Tabs';
import Input from 'custom-elements/Input/Input';

import './BonusesOption.scss';

// Псевдокалькулятор
const randomIntFromInterval = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

const BonusesOption = ({
  title,
  description,
  extendedLink,
  tabs,
  tabsFields,
  onClick,
}) => {
  const [activeTab, setActiveTab] = useState(tabs[0].value);
  const [numbers, setNumbers] = useState({});
  const [activeFields, setActiveFields] = useState(
    tabsFields.find(item => item.section === activeTab),
  );

  const [sale, setSale] = useState(randomIntFromInterval(15, 35));
  const [payment, setPayment] = useState(randomIntFromInterval(3000, 20000));
  const [rate, setRate] = useState(randomIntFromInterval(10, 20));

  const handleTabClick = value => {
    setActiveTab(value);
    setActiveFields(tabsFields.find(item => item.section === value));
  };

  const handleInputChange = ({ id, value }) => {
    setNumbers({
      ...numbers,
      [id]: value,
    });

    setSale(randomIntFromInterval(15, 35));
    setPayment(randomIntFromInterval(3000, 20000));
    setRate(randomIntFromInterval(10, 20));
  };

  return (
    <div className="bonuses-option">
      <div className="bonuses-option__container">
        <div className="bonuses-option__title">
          <Title title={title} level={3} weight="normal" />
        </div>

        <p className="bonuses-option__description">
          {description}
        </p>

        <a
          href={extendedLink}
          className="bonuses-option__link"
          target="_blank"
          rel="noopener noreferrer"
        >
          Подробнее об опции
        </a>

        <div className="bonuses-option__tabs-wrapper">
          <div className="bonuses-option__tabs">
            <Tabs
              items={tabs}
              activeTab={activeTab}
              onClick={handleTabClick}
            />
          </div>
        </div>

        <div className="bonuses-option__fields">
          {activeFields?.inputs?.map(input => (
            <Input
              key={input.id}
              id={input.id}
              type="text"
              label={input.placeholder}
              value={
                typeof numbers[input.id] === 'string'
                  ? numbers[input.id]
                  : input.defaultValue.toString()
              }
              caption={input.caption}
              maskType={input.maskType}
              onChange={handleInputChange}
            />
          ))}
        </div>
      </div>

      <BonusesSummarise
        sale={sale.toString()}
        payment={payment.toString()}
        rate={rate.toString()}
        onClick={onClick}
      />
    </div>
  );
};

BonusesOption.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  extendedLink: PropTypes.string.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    disabled: PropTypes.bool,
  })).isRequired,
  tabsFields: PropTypes.arrayOf(PropTypes.shape({
    section: PropTypes.string,
    inputs: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      placeholder: PropTypes.string,
      defaultValue: PropTypes.string,
      caption: PropTypes.string,
      maskType: PropTypes.string,
    })),
  })).isRequired,
  onClick: PropTypes.func,
};

BonusesOption.defaultProps = {
  onClick: () => {},
};

export default BonusesOption;
