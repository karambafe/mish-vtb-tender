import React from 'react';

import { ReactComponent as FacebookIcon } from 'assets/images/footer/facebook.svg';
import { ReactComponent as TwitterIcon } from 'assets/images/footer/twitter.svg';
import { ReactComponent as VkIcon } from 'assets/images/footer/vk.svg';
import { ReactComponent as YoutubeIcon } from 'assets/images/footer/youtube.svg';
import { ReactComponent as InstagramIcon } from 'assets/images/footer/instagram.svg';
import { ReactComponent as OkIcon } from 'assets/images/footer/ok.svg';
import { ReactComponent as AppStoreIcon } from 'assets/images/footer/app-store.svg';
import { ReactComponent as GooglePlayIcon } from 'assets/images/footer/google-play.svg';

import './Footer.scss';

const LINKS = [{
  title: 'Карта сайта',
  link: 'https://www.vtb.ru/sitemap/',
}, {
  title: 'О сайте',
  link: 'https://www.vtb.ru/o-sajte/',
}, {
  title: 'Безопасность',
  link: 'https://www.vtb.ru/bezopasnost/',
}];

const SOCIALS = [{
  label: 'Facebook',
  link: 'https://www.facebook.com/vtbrussia/',
  icon: FacebookIcon,
}, {
  label: 'Twitter',
  link: 'https://twitter.com/VTB',
  icon: TwitterIcon,
}, {
  label: 'Вконтакте',
  link: 'https://vk.com/vtb',
  icon: VkIcon,
}, {
  label: 'Youtube',
  link: 'https://www.youtube.com/user/vtbgroup',
  icon: YoutubeIcon,
}, {
  label: 'Instagram',
  link: 'https://www.instagram.com/bankvtb/',
  icon: InstagramIcon,
}, {
  label: 'Одноклассники',
  link: 'https://ok.ru/vtb',
  icon: OkIcon,
}];

const STORES = [{
  label: 'Apple store',
  link: 'https://apps.apple.com/app/apple-store/id472951966?app',
  icon: AppStoreIcon,
}, {
  label: 'Google play market',
  link: 'https://play.google.com/store/apps/details?id=ru.vtb24.mobilebanking.android&referrer=utm_source%3Dsite%26utm_medium%3Dreferral%26utm_campaign%3Dsite_footer_vtbonline_android_install%26anid%3Dadmob&',
  icon: GooglePlayIcon,
}];

export default () => (
  <footer className="footer">
    <div className="footer__wrapper">
      <div className="footer__top-container">
        <div className="footer__info-container">
          <p className="footer__copyright">
            190000, г. Санкт-Петербург, ул. Большая Морская, д. 29
            <br />
            Генеральная лицензия Банка России №1000
            <br />
            Ⓒ ВТБ, 2020
          </p>

          <ul className="footer__list footer__list_links">
            {LINKS.map(item => (
              <li key={item.title} className="footer__item footer__item_links">
                <a
                  href={item.link}
                  className="footer__link"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {item.title}
                </a>
              </li>
            ))}
          </ul>
        </div>

        <div className="footer__socials-container">
          <ul className="footer__list footer__list_socials">
            {SOCIALS.map(item => (
              <li key={item.label} className="footer__item footer__item_socials">
                <a
                  href={item.link}
                  aria-label={item.label}
                  className="footer__link"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <item.icon />
                </a>
              </li>
            ))}
          </ul>

          <div className="footer__stores-container">
            Мобильные приложения

            <ul className="footer__list footer__list_stores">
              {STORES.map(item => (
                <li key={item.label} className="footer__item footer__item_stores">
                  <a
                    href={item.link}
                    aria-label={item.label}
                    className="footer__link"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <item.icon />
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>

      <aside className="footer__cookies">
        Для повышения удобства работы с сайтом Банк ВТБ&nbsp;
        <a
          href="https://www.vtb.ru/soglasie-na-obrabotku-dannyh-152-fz/"
          className="footer__link footer__link_cookie"
          target="_blank"
          rel="noopener noreferrer"
        >
          использует файлы cookie
        </a>
        . В cookie содержатся данные о прошлых посещениях сайта.
        Если вы не хотите, чтобы эти данные обрабатывались,
        отключите cookie в настройках браузера.
      </aside>
    </div>
  </footer>
);
