import React from 'react';
import PropTypes from 'prop-types';

import Title from 'custom-elements/Title/Title';

import './FaqItem.scss';

const FaqItem = ({
  label,
  description,
  extendedLink,
  value,
}) => (
  <li className={`faq-item faq-item_${value}`}>
    <div className="faq-item__title">
      <Title title={label} level={3} weight="normal" />
    </div>

    <p className="faq-item__description">
      {description}
    </p>

    {extendedLink && (
      <a
        href={extendedLink}
        className="faq-item__link"
        target="_blank"
        rel="noopener noreferrer"
      >
        Подробнее
      </a>
    )}
  </li>
);

FaqItem.propTypes = {
  label: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  extendedLink: PropTypes.string,
};

FaqItem.defaultProps = {
  extendedLink: '',
};

export default FaqItem;
