import React from 'react';

import HeaderTop from 'components/HeaderTop/HeaderTop';
import HeaderMain from 'components/HeaderMain/HeaderMain';

import './Header.scss';

export default () => (
  <header className="header">
    <div className="header__wrapper">
      <HeaderTop />

      <HeaderMain />
    </div>
  </header>
);
