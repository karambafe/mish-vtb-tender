import React from 'react';

import Collapse from 'custom-elements/Collapse/Collapse';

import './HowOrderCard.scss';

const ITEMS = [{
  title: 'Заполните форму заявки',
  description: 'Это займет не более 5 минут',
}, {
  title: 'Выберите способ получения',
  description: 'В отделении или, для жителей Москвы и Санкт-Петербурга, курьером',
}, {
  title: 'Получите карту',
  description: 'Мы сообщим по SMS, как только карта будет готова. Не забудьте паспорт',
}];

const HowOrderCard = () => (
  <Collapse label="Как заказать карту">
    <ul className="how-order-card">
      {ITEMS.map((item, index) => (
        <li key={item.title} className="how-order-card__item">
          <div className="how-order-card__order">
            <div className="how-order-card__index">{index + 1}</div>

            {index !== ITEMS.length - 1 && (
              <div className="how-order-card__line" />
            )}
          </div>

          <div className="how-order-card__info">
            <h4 className="how-order-card__title">{item.title}</h4>

            <p className="how-order-card__description">{item.description}</p>
          </div>
        </li>
      ))}
    </ul>
  </Collapse>
);

export default HowOrderCard;
