import React, { useState } from 'react';

import { ReactComponent as EyeIcon } from 'assets/images/header/eye.svg';
import { ReactComponent as ArrowDownIcon } from 'assets/images/arrow-down.svg';

import './HeaderTop.scss';

export default () => {
  const [lang, setLang] = useState('En'); // eslint-disable-line no-unused-vars
  const [city, setCity] = useState('Москва'); // eslint-disable-line no-unused-vars

  return (
    <div className="header-top">
      <div className="header-top__geolocation-container">
        <button type="button" className="header-top__button header-top__button_city">
          {city}
          <ArrowDownIcon />
        </button>

        <div className="header-top__offices">
          <a
            href="https://www.vtb.ru/o-banke/kontakty/otdeleniya"
            className="header-top__link"
            target="_blank"
            rel="noopener noreferrer"
          >
            Отделения
          </a>
          &nbsp;и&nbsp;
          <a
            href="https://www.vtb.ru/o-banke/kontakty/bankomaty"
            className="header-top__link"
            target="_blank"
            rel="noopener noreferrer"
          >
            банкоматы
          </a>
        </div>
      </div>

      <div className="header-top__vision-and-language">
        <button type="button" className="header-top__button header-top__button_vision">
          <EyeIcon />

          <span className="header-top__vision-title">Версия для слабовидящих</span>
        </button>

        <button type="button" className="header-top__button header-top__button_language">
          {lang}
        </button>
      </div>
    </div>
  );
};
