import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { throttle } from 'lodash';

import Tabs from 'custom-elements/Tabs/Tabs';
import Button from 'custom-elements/Button/Button';

import './NavigationBar.scss';

const TABS = [
  { label: 'О карте', value: 'card' },
  { label: 'Тарифы', value: 'rates' },
  { label: 'Вопросы и ответы', value: 'faq' },
];

const SWIPER_BREAKPOINTS_RULES = {
  320: {
    spaceBetween: 24,
  },
  768: {
    spaceBetween: 40,
  },
};

const NavigationBar = ({ activeTab, onButtonClick, onTabClick }) => {
  const [isInTop, setIsInTop] = useState(false);

  const navigationBar = useRef(null);

  const handleScroll = throttle(() => {
    setIsInTop(navigationBar?.current?.getBoundingClientRect()?.top <= 0);
  }, 100);

  const handleTabClick = value => {
    onTabClick(value);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => window.removeEventListener('scroll', handleScroll);
  });

  return (
    <div
      className={`navigation-bar ${isInTop ? 'navigation-bar_top' : ''}`}
      ref={navigationBar}
    >
      <div className={`navigation-bar__wrapper ${isInTop ? 'navigation-bar__wrapper_top' : ''}`}>
        <div className="navigation-bar__tabs-container">
          <Tabs
            items={TABS}
            activeTab={activeTab}
            extendedTabClass="navigation-bar__tab"
            swiperBreakpointsRules={SWIPER_BREAKPOINTS_RULES}
            onClick={handleTabClick}
          />
        </div>

        <div className={`navigation-bar__button ${isInTop ? 'navigation-bar__button_top' : ''}`}>
          <Button
            label="Заказать карту"
            color="red"
            onClick={onButtonClick}
          />
        </div>
      </div>
    </div>
  );
};

NavigationBar.propTypes = {
  activeTab: PropTypes.string.isRequired,
  onButtonClick: PropTypes.func,
  onTabClick: PropTypes.func,
};

NavigationBar.defaultProps = {
  onButtonClick: () => {},
  onTabClick: () => {},
};

export default NavigationBar;
