import React from 'react';

import Title from 'custom-elements/Title/Title';

import './FaqHowToUse.scss';

export default () => (
  <div className="faq-how-to-use">
    <div className="faq-how-to-use__title">
      <Title title="Как пользоваться картой" level={3} weight="normal" />
    </div>

    <ul className="faq-how-to-use__list">
      <li className="faq-how-to-use__item">
        <div className="faq-how-to-use__order">
          <div className="faq-how-to-use__index">1</div>

          <div className="faq-how-to-use__line" />
        </div>

        <div className="faq-how-to-use__info">
          <h4 className="faq-how-to-use__item-title">Получите ПИН-код</h4>

          <p className="faq-how-to-use__description">
            В&nbsp;
            <a
              href="https://online.vtb.ru"
              className="faq-how-to-use__link"
              target="_blank"
              rel="noopener noreferrer"
            >
              ВТБ-Онлайн
            </a>
            &nbsp;или по телефону&nbsp;
            <a
              href="tel:+78001002424"
              className="faq-how-to-use__link"
            >
              8&nbsp;(800)&nbsp;100-24-24
            </a>
            &nbsp;(круглосуточно)
          </p>
        </div>
      </li>

      <li className="faq-how-to-use__item">
        <div className="faq-how-to-use__order">
          <div className="faq-how-to-use__index">2</div>

          <div className="faq-how-to-use__line" />
        </div>

        <div className="faq-how-to-use__info">
          <h4 className="faq-how-to-use__item-title">
            Активируйте карту в&nbsp;
            <a
              href="https://www.vtb.ru/o-banke/kontakty/otdeleniya/"
              className="faq-how-to-use__link"
              target="_blank"
              rel="noopener noreferrer"
            >
              банкомате ВТБ
            </a>
          </h4>

          <p className="faq-how-to-use__description">
            Для этого просто запросите баланс
          </p>
        </div>
      </li>

      <li className="faq-how-to-use__item">
        <div className="faq-how-to-use__order">
          <div className="faq-how-to-use__index">3</div>
        </div>

        <div className="faq-how-to-use__info">
          <h4 className="faq-how-to-use__item-title">
            Загрузите карту в кошелек PAY
          </h4>

          <p className="faq-how-to-use__description">
            Чтобы не носить карту с собой, а расплачиваться смартфоном
          </p>
        </div>
      </li>
    </ul>
  </div>
);
