/* eslint-disable react/destructuring-assignment */
import React from 'react';
import animateScrollTo from 'animated-scroll-to';

import FormRadioList from 'components/FormRadioList/FormRadioList';
import FormGender from 'components/FormGender/FormGender';
import FormSubmitButton from 'components/FormSubmitButton/FormSubmitButton';

import Input from 'custom-elements/Input/Input';
import Checkbox from 'custom-elements/Checkbox/Checkbox';
import Select from 'custom-elements/Select/Select';

import {
  validateFullname,
  validateEmail,
  validateDate,
  validatePhone,
  validateCadName,
  validateKeyword,
} from './validate';

import {
  COUNTRIES,
  CITIES,
  PAYMENT_SYSTEMS,
  CURRENCIES,
  GENDERS,
  CARD_NAME_INFO,
  KEYWORD_INFO,
} from './data';

import './Form.scss';

class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fullname: '',
      email: '',
      date: '',
      phone: '',
      cardName: '',
      keyword: '',
      country: 'ru',
      city: 'moscow',
      paymentSystem: 'visa',
      currency: 'rub',
      gender: 'male',
      errors: {},
    };

    this.fullname = React.createRef();
    this.email = React.createRef();
    this.date = React.createRef();
    this.phone = React.createRef();
    this.cardName = React.createRef();
    this.keyword = React.createRef();
  }

  get hasErrors() {
    const errors = Object.keys(this.state.errors);
    if (!errors.length) return false;
    return errors.some(key => this.state.errors[key]);
  }

  handleInputChange = ({ id, value }) => {
    this.setState(prevState => ({
      [id]: id === 'cardName' ? value.toUpperCase() : value,
      errors: {
        ...prevState.errors,
        [id]: '',
      },
    }));
  }

  handlePaymentSystemsChange = paymentSystem => {
    this.setState({ paymentSystem });
  }

  handleCurrenciesChange = currency => {
    this.setState({ currency });
  }

  handleGenderChange = gender => {
    this.setState({ gender });
  }

  handleCountriesChange = country => {
    this.setState({ country });
  }

  handleCitiesChange = city => {
    this.setState({ city });
  }

  handlePhoneButtonClick = () => {
    this.setState(prevState => ({
      errors: {
        ...prevState.errors,
        phone: validatePhone(prevState.phone),
      },
    }), () => {
      if (this.state.errors.phone) return;

      window.confirm('Номер корректный'); // eslint-disable-line no-alert
    });
  }

  handleSubmit = e => {
    e.preventDefault();

    this.setState(prevState => ({
      errors: {
        fullname: validateFullname(prevState.fullname),
        email: validateEmail(prevState.email),
        date: validateDate(prevState.date),
        phone: validatePhone(prevState.phone),
        cardName: validateCadName(prevState.cardName),
        keyword: validateKeyword(prevState.keyword),
      },
    }), () => {
      if (!this.hasErrors) {
        window.confirm('Форма успешно отправлена'); // eslint-disable-line no-alert
        return;
      }

      const firstErrorKey = Object
        .keys(this.state.errors)
        .filter(key => this.state.errors[key])[0];
      animateScrollTo(this[firstErrorKey].current, { verticalOffset: -100 });
    });
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleSubmit} noValidate>
        <div ref={this.fullname}>
          <Input
            id="fullname"
            type="text"
            label="ФИО"
            autocomplete="name"
            value={this.state.fullname}
            error={this.state.errors.fullname || ''}
            onChange={this.handleInputChange}
          />
        </div>

        <div ref={this.email}>
          <Input
            id="email"
            type="email"
            label="Email"
            autocomplete="email"
            value={this.state.email}
            error={this.state.errors.email || ''}
            caption="На этот адрес вы получите письмо с информацией о заявке"
            onChange={this.handleInputChange}
          />
        </div>

        <div ref={this.date} className="form__row">
          <Input
            id="date"
            type="text"
            maskType="date"
            label="Дата рождения"
            value={this.state.date}
            error={this.state.errors.date || ''}
            onChange={this.handleInputChange}
          />

          <div className="form__block">
            <FormGender
              activeItem={this.state.gender}
              items={GENDERS}
              onChange={this.handleGenderChange}
            />
          </div>
        </div>

        <div ref={this.phone} className="form__phone">
          <Input
            id="phone"
            type="tel"
            label="Телефон"
            autocomplete="tel"
            maskType="phone"
            value={this.state.phone}
            error={this.state.errors.phone || ''}
            caption="На этот номер вы получите SMS с решением банка"
            onChange={this.handleInputChange}
          />

          {this.state.phone && (
            <button
              type="button"
              className="form__phone-button"
              onClick={this.handlePhoneButtonClick}
            >
              Проверить
            </button>
          )}
        </div>

        <div className="form__block">
          <Select
            id="countries"
            label="Гражданство"
            activeOption={this.state.country}
            options={COUNTRIES}
            onChange={this.handleCountriesChange}
          />
        </div>

        <div className="form__block">
          <Select
            id="cities"
            label="Город получения карты"
            activeOption={this.state.city}
            options={CITIES}
            onChange={this.handleCitiesChange}
          />
        </div>

        <div className="form__row">
          <div className="form__block">
            <FormRadioList
              title="Платежная система"
              items={PAYMENT_SYSTEMS}
              activeItem={this.state.paymentSystem}
              onChange={this.handlePaymentSystemsChange}
            />
          </div>

          <div className="form__block">
            <FormRadioList
              title="Валюта"
              items={CURRENCIES}
              activeItem={this.state.currency}
              onChange={this.handleCurrenciesChange}
            />
          </div>
        </div>

        <div ref={this.cardName}>
          <Input
            id="cardName"
            type="text"
            label="Имя на карте"
            value={this.state.cardName}
            error={this.state.errors.cardName || ''}
            info={CARD_NAME_INFO}
            onChange={this.handleInputChange}
          />
        </div>

        <div ref={this.cardName}>
          <Input
            id="keyword"
            type="text"
            label="Ключевое слово"
            value={this.state.keyword}
            error={this.state.errors.keyword || ''}
            info={KEYWORD_INFO}
            onChange={this.handleInputChange}
          />
        </div>

        <div className="form__agreement">
          <div className="form__agreement-checkbox">
            <Checkbox
              checked
              disabled
              name="agreement"
              value="agreement"
            />
          </div>

          <p className="form__agreement-text">
            Я соглашаюсь с
            <a
              href="https://vtb.ru"
              className="form__agreement-link"
              target="_blank"
              rel="noopener noreferrer"
            >
              условиями обработки и использования
            </a>
            моих персональных данных
          </p>
        </div>

        <div className="form__submit-button">
          <FormSubmitButton />
        </div>
      </form>
    );
  }
}

export default Form;
