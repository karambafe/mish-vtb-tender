import visaImage from 'assets/images/form/visa.png';
import mastercardImage from 'assets/images/form/mastercard.png';
import mirImage from 'assets/images/form/mir.png';
import rubImage from 'assets/images/form/rub.png';
import usdImage from 'assets/images/form/usd.png';
import eurImage from 'assets/images/form/eur.png';

export const COUNTRIES = [
  { title: 'Российская Федерация', value: 'ru' },
  { title: 'Казахстан', value: 'kz' },
  { title: 'Узбекистан', value: 'uz' },
];

export const CITIES = [
  { title: 'Москва', value: 'moscow' },
  { title: 'Саратов', value: 'saratov' },
  { title: 'Новгород', value: 'novgorod' },
];

export const PAYMENT_SYSTEMS = [
  { value: 'visa', image: { src: visaImage, width: '38', height: '12' } },
  { value: 'mastercard', image: { src: mastercardImage, width: '116', height: '21' } },
  { value: 'mir', image: { src: mirImage, width: '51', height: '19' } },
];

export const CURRENCIES = [
  { value: 'rub', title: 'Рубли', image: { src: rubImage, width: '10', height: '13' } },
  { value: 'usd', title: 'Доллары США', image: { src: usdImage, width: '8', height: '17' } },
  { value: 'eur', title: 'Евро', image: { src: eurImage, width: '10', height: '13' } },
];

export const GENDERS = [
  { value: 'male', label: 'Мужской' },
  { value: 'female', label: 'Женский' },
];

export const CARD_NAME_INFO = `
  Укажите имя на латинице, как в загранпаспорте. <br>
  Если длина имени и фамилии больше 19 символов, то вместо полного 
  имени используйте только первую букву. <br>
  Например, A MALINOVSKAYA Если у вас двойные имя или фамилия, 
  то укажите их через дефис. Например, OLGA IVANOVA-SOKOL.
`;

export const KEYWORD_INFO = `
  Придумайте слово, которое вы будете называть для идентификации при звонке в банк. <br>
  Слово должно быть на русском языке без цифр.
`;
