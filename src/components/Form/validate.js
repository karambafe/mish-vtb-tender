const EMAIL_REGEXP = /\S+@\S+\.\S+/;
const CARD_NAME_REGEXP = /^[a-zA-Z]+$/;
const KEYWORD_REGEXP = /^[а-яА-Я]+$/;

const REQUIRED_FILED = 'Обязательное поле';

export const validateFullname = fullname => {
  const fullnameTrimmed = fullname.trim();

  if (!fullnameTrimmed) return REQUIRED_FILED;
  if (fullnameTrimmed.split(' ').length !== 3) return 'ФИО должно состоять из 3ех частей';
  return '';
};

export const validateEmail = email => {
  const emailTrimmed = email.trim();

  if (!emailTrimmed) return REQUIRED_FILED;
  if (!emailTrimmed.includes('@')) return 'Email должен содержать символ "@"';
  if (!emailTrimmed.match(EMAIL_REGEXP)) return 'Неверный формат или недопустимые символы в адресе';
  return '';
};

export const validateDate = date => {
  const dateTrimmed = date.trim();

  if (!dateTrimmed) return REQUIRED_FILED;
  if (dateTrimmed.length < 8) return 'Необходимо заполнить полностью';
  return '';
};

export const validatePhone = phone => {
  if (!phone) return REQUIRED_FILED;
  if (phone.length < 11) return 'Номер введен неверно';
  return '';
};

export const validateCadName = cardName => {
  const cardNameTrimmed = cardName.trim();

  if (!cardNameTrimmed) return REQUIRED_FILED;
  if (cardNameTrimmed.split(' ').length !== 2) return 'Имя должно состоять из 2ух частей';
  if (!cardNameTrimmed.replace(/ /g, '').match(CARD_NAME_REGEXP)) {
    return 'Имя должно состоять только из латинских символов';
  }
  return '';
};

export const validateKeyword = keyword => {
  const keywordTrimmed = keyword.trim();

  if (!keywordTrimmed) return REQUIRED_FILED;
  if (!keywordTrimmed.match(KEYWORD_REGEXP)) return 'Слово должно быть на русском языке';
  return '';
};
