import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Title from 'custom-elements/Title/Title';
import Button from 'custom-elements/Button/Button';

import './RatesItem.scss';

const RatesItem = ({
  label,
  list,
  isGray,
  isSimple,
  onClick,
}) => (
  <li className={classNames('rates-item', {
    'rates-item_color_gray': isGray,
    'rates-item_simple': isSimple,
  })}
  >
    <div className="rates-item__label">
      {label}
    </div>

    <ul className="rates-item__nested-list">
      {list.map(item => (
        <li key={`${label}-${item.description}`} className="rates-item__nested-item">
          {item.title && (
            <div className="rates-item__nested-title">
              <Title title={item.title} weight="thin" level={2} />
            </div>
          )}

          {item.description && (
            <p className={classNames('rates-item__nested-description', {
              'rates-item__nested-description_color_black': item.isDescriptionBlack,
              'rates-item__nested-description_caption': item.isCaption,
            })}
            >
              {item.description}
            </p>
          )}

          {item.button && (
            <div className="rates-item__nested-button-container">
              <Button label={item.button.title} onClick={() => onClick(item.button.value)} />
            </div>
          )}
        </li>
      ))}
    </ul>
  </li>
);

RatesItem.propTypes = {
  label: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    isDescriptionBlack: PropTypes.bool,
    isCaption: PropTypes.bool,
    button: PropTypes.shape({
      title: PropTypes.string,
      value: PropTypes.string,
    }),
  })).isRequired,
  isGray: PropTypes.bool,
  isSimple: PropTypes.bool,
  onClick: PropTypes.func,
};

RatesItem.defaultProps = {
  isGray: false,
  isSimple: false,
  onClick: () => {},
};

export default RatesItem;
