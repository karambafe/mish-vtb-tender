import React from 'react';
import PropTypes from 'prop-types';

import Title from 'custom-elements/Title/Title';

import './PocketBankItem.scss';

const PocketBankItem = ({ title, description, children }) => (
  <li className="pocket-bank-item">
    <div className="pocket-bank-item__icon-container">
      <div className="pocket-bank-item__icon">
        {children}
      </div>
    </div>

    <div className="pocket-bank-item__content-container">
      <div className="pocket-bank-item__title">
        <Title title={title} level={4} weight="normal" />
      </div>

      <p className="pocket-bank-item__description">{description}</p>
    </div>
  </li>
);

PocketBankItem.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
};

export default PocketBankItem;
