import React from 'react';
import PropTypes from 'prop-types';

import Radio from 'custom-elements/Radio/Radio';

import './FormRadioList.scss';

const FormRadioList = ({
  title,
  items,
  activeItem,
  onChange,
}) => (
  <div className="form-radio-list">
    <h3 className="form-radio-list__title">{title}</h3>

    <ul className="form-radio-list__list">
      {items.map(item => (
        <li key={item.value} className="form-radio-list__item">
          <Radio
            title={item.title}
            image={item.image}
            value={item.value}
            name={item.value}
            checked={item.value === activeItem}
            onChange={onChange}
          />
        </li>
      ))}
    </ul>
  </div>
);

FormRadioList.propTypes = {
  title: PropTypes.string.isRequired,
  activeItem: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    image: PropTypes.object,
    value: PropTypes.string,
  })).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FormRadioList;
