import React from 'react';
import PropTypes from 'prop-types';

import Title from 'custom-elements/Title/Title';
import Button from 'custom-elements/Button/Button';

import './BonusesSummarise.scss';

const formatNumber = value => value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, ' ');

const BonusesSummarise = ({
  sale,
  payment,
  rate,
  onClick,
}) => (
  <div className="bonuses-summarise">
    <div className="bonuses-summarise__title">
      <Title title="Итоговая награда за год" weight="normal" level={3} />
    </div>

    <div className="bonuses-summarise__container">
      <div className="bonuses-summarise__number-container">
        <div className="bonuses-summarise__number bonuses-summarise__number_color_green">
          {sale}
          %
        </div>

        <div className="bonuses-summarise__description">
          скидка к ставке
        </div>
      </div>

      <div className="bonuses-summarise__number-container">
        <div className="bonuses-summarise__number">
          {formatNumber(payment)}
          ₽
        </div>

        <div className="bonuses-summarise__description">
          ежемесячный платеж
        </div>
      </div>

      <div className="bonuses-summarise__number-container">
        <div className="bonuses-summarise__number">
          {rate}
          %
        </div>

        <div className="bonuses-summarise__description">
          ставка по кредиту
        </div>
      </div>
    </div>

    <div className="bonuses-summarise__button-container">
      <Button
        label="Выбрать опцию и заказать карту"
        color="red"
        onClick={onClick}
      />
    </div>

    <div className="bonuses-summarise__caption">
      Информация носит справочный характер и не является публичной офертой
    </div>
  </div>
);

BonusesSummarise.propTypes = {
  sale: PropTypes.string.isRequired,
  payment: PropTypes.string.isRequired,
  rate: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

BonusesSummarise.defaultProps = {
  onClick: () => {},
};

export default BonusesSummarise;
