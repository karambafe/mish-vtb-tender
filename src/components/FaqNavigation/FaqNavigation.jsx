import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Button from 'custom-elements/Button/Button';

import './FaqNavigation.scss';

const FaqNavigation = ({
  activeTab,
  items,
  onTabClick,
  onButtonClick,
}) => (
  <div className="faq-navigation">
    <nav className="faq-navigation__container">
      {items?.map(item => (
        <button
          key={item.value}
          type="button"
          className={classNames('faq-navigation__item', {
            'faq-navigation__item_active': activeTab === item.value,
          })}
          disabled={item.disabled}
          onClick={() => onTabClick(item.value)}
        >
          {item.label}
        </button>
      ))}
    </nav>

    <div className="faq-navigation__button-container">
      <Button label="Все вопросы и ответы" color="blue" onClick={onButtonClick} />
    </div>
  </div>
);

FaqNavigation.propTypes = {
  activeTab: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    disabled: PropTypes.bool,
  })).isRequired,
  onButtonClick: PropTypes.func,
  onTabClick: PropTypes.func,
};

FaqNavigation.defaultProps = {
  onButtonClick: () => {},
  onTabClick: () => {},
};

export default FaqNavigation;
