import React, { useState } from 'react';
import classNames from 'classnames';

import { ReactComponent as ArrowDownIcon } from 'assets/images/arrow-down.svg';
import { ReactComponent as OnlineIcon } from 'assets/images/header/online.svg';
import { ReactComponent as LoggIcon } from 'assets/images/header/logo.svg';

import './HeaderMain.scss';

const LIST = [{
  title: 'Кредиты',
}, {
  title: 'Карты',
  link: 'https://www.vtb.ru/personal/karty/',
  active: true,
}, {
  title: 'Ипотека',
  link: 'https://www.vtb.ru/personal/ipoteka/',
}, {
  title: 'Автокредиты',
  link: 'https://www.vtb.ru/personal/avtokredity/',
}, {
  title: 'Вклады и счета',
  link: 'https://www.vtb.ru/personal/vklady-i-scheta/',
}, {
  title: 'Инвестиции',
  link: 'https://www.vtb.ru/personal/investicii/',
}];

const TOP_LIST = [{
  title: 'Малый и средний бизнес',
  link: 'https://www.vtb.ru/malyj-biznes/',
}, {
  title: 'Крупный бизнес',
  link: 'https://www.vtb.ru/krupnyj-biznes/',
}, {
  title: 'Финансовым учреждениям',
  link: 'https://www.vtb.ru/finansovye-uchrezhdeniya/#tab_0_1#',
}, {
  title: 'Акционерам и инвесторам',
  link: 'https://www.vtb.ru/akcionery-i-investory/',
}, {
  title: 'О группе ВТБ',
  link: 'https://www.vtb.ru/o-banke/',
}];

export default () => {
  // eslint-disable-next-line no-unused-vars
  const [category, setCategory] = useState('Частным лицам');

  return (
    <div className="header-main">
      <a
        href="https://www.vtb.ru/app"
        className="header-main__online-link"
        target="_blank"
        rel="noopener noreferrer"
      >
        <OnlineIcon />

        ВТБ-Онлайн
      </a>

      <div className="header-main__top-container">
        <button type="button" className="header-main__category-button">
          {category}

          <ArrowDownIcon />
        </button>

        <ul className="header-main__top-list">
          {TOP_LIST.map(item => (
            <li key={item.title} className="header-main__top-item">
              <a
                href={item.link}
                className="header-main__link header-main__link_color_white"
                target="_blank"
                rel="noopener noreferrer"
              >
                {item.title}
              </a>
            </li>
          ))}
        </ul>
      </div>

      <div className="header-main__bottom-container">
        <a href="/" className="header-main__logo" aria-label="На главную">
          <LoggIcon />
        </a>

        <ul className="header-main__bottom-list">
          {LIST.map(item => (
            <li key={item.title} className="header-main__bottom-item">
              <a
                href={item.link}
                className={classNames('header-main__link', {
                  'header-main__link_active': item.active,
                })}
                target="_blank"
                rel="noopener noreferrer"
              >
                {item.title}
              </a>
            </li>
          ))}
        </ul>

        <button type="button" className="header-main__more-button">
          Еще

          <span className="header-main__more-button-icon">
            <ArrowDownIcon />
          </span>
        </button>
      </div>
    </div>
  );
};
