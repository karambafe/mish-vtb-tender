import React from 'react';

import RequiredDocuments from 'components/RequiredDocuments/RequiredDocuments';
import HowOrderCard from 'components/HowOrderCard/HowOrderCard';

import Collapse from 'custom-elements/Collapse/Collapse';

import './MulticardRequestInfo.scss';

const MulticardRequestInfo = () => (
  <ul className="multicard-request-info">
    <li className="multicard-request-info__item">
      <HowOrderCard />
    </li>

    <li className="multicard-request-info__item">
      <RequiredDocuments />
    </li>

    <li className="multicard-request-info__item">
      <Collapse label="Условия оформления">
        Информация об условиях оформления карты
      </Collapse>
    </li>

    <li className="multicard-request-info__item">
      <Collapse label="Курьерская доставка">
        Информация о курьерской доставке
      </Collapse>
    </li>
  </ul>
);

export default MulticardRequestInfo;
