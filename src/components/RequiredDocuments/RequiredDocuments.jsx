import React from 'react';

import Collapse from 'custom-elements/Collapse/Collapse';

import { ReactComponent as DotIcon } from 'assets/images/required-documents-dot.svg';

import './RequiredDocuments.scss';

const ITEMS = [{
  title: 'Для граждан РФ',
  description: 'Паспорт РФ',
}, {
  title: 'Для иностранных граждан',
  description: `Для оформления карты обратитесь в отделение.
    Более подробная информация— по телефону 
    <a href="tel:+78001002424" class="required-documents__link">8&nbsp;(800)&nbsp;100-24-24</a>
    (круглосуточно)`,
}];

const RequiredDocuments = () => (
  <Collapse label="Необходимые документы">
    <ul className="required-documents">
      {ITEMS.map(item => (
        <li key={item.title} className="required-documents__item">
          <h4 className="required-documents__title">{item.title}</h4>

          <p
            className="required-documents__description"
            dangerouslySetInnerHTML={{ __html: item.description }}
          />

          <span className="required-documents__icon">
            <DotIcon />
          </span>
        </li>
      ))}
    </ul>
  </Collapse>
);

export default RequiredDocuments;
