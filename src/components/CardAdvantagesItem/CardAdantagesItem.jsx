import React from 'react';
import PropTypes from 'prop-types';

import Title from 'custom-elements/Title/Title';

import './CardAdvantagesItem.scss';

const CardAdvantagesItem = ({
  mainTitle,
  mainDescription,
  secondaryTitle,
  secondaryDescription,
  iconDescription,
  children,
}) => (
  <li className="card-advantages-item">
    <div className="card-advantages-item__container">
      <div className="card-advantages-item__text-block">
        <Title title={mainTitle} weight="bold" level={3} />

        <p className="card-advantages-item__description">
          { mainDescription }
        </p>
      </div>

      <div className="card-advantages-item__text-block">
        <Title title={secondaryTitle} weight="bold" level={3} />

        <p className="card-advantages-item__description">
          { secondaryDescription }
        </p>
      </div>
    </div>

    <div className="card-advantages-item__icon-container">
      <div className="card-advantages-item__icon">
        { children }
      </div>

      <p className="card-advantages-item__icon-description">
        { iconDescription }
      </p>
    </div>
  </li>
);

CardAdvantagesItem.propTypes = {
  mainTitle: PropTypes.string.isRequired,
  mainDescription: PropTypes.string.isRequired,
  secondaryTitle: PropTypes.string.isRequired,
  secondaryDescription: PropTypes.string.isRequired,
  iconDescription: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
};

export default CardAdvantagesItem;
