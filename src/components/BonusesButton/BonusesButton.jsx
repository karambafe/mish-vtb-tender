import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './BonusesButton.scss';

const BonusesButton = ({
  label,
  value,
  description,
  children,
  activeValue,
  disabled,
  onClick,
}) => (
  <button
    type="button"
    className={classNames('bonuses-button', {
      'bonuses-button_active': activeValue === value,
    })}
    disabled={disabled}
    onClick={() => onClick(value)}
  >
    <span className="bonuses-button__icon">
      {children}
    </span>

    <span className="bonuses-button__container">
      <span className="bonuses-button__label">
        {label}
      </span>

      <span className="bonuses-button__description">
        {description}
      </span>
    </span>
  </button>
);

BonusesButton.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  activeValue: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

BonusesButton.defaultProps = {
  disabled: false,
  onClick: () => {},
};

export default BonusesButton;
