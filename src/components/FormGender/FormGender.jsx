import React from 'react';
import PropTypes from 'prop-types';

import Tabs from 'custom-elements/Tabs/Tabs';

import './FormGender.scss';

const SWIPER_BREAKPOINTS_RULES = {
  320: {
    spaceBetween: 24,
  },
};

const FormGender = ({ items, activeItem, onChange }) => (
  <div className="form-gender">
    <h3 className="form-gender__title">Пол</h3>

    <div className="form-gender__tabs">
      <Tabs
        items={items}
        activeTab={activeItem}
        extendedTabClass="form-gender__tab"
        swiperBreakpointsRules={SWIPER_BREAKPOINTS_RULES}
        onClick={onChange}
      />
    </div>
  </div>
);

FormGender.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    value: PropTypes.string,
  })).isRequired,
  activeItem: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FormGender;
