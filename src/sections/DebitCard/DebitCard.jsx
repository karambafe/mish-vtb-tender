import React from 'react';
import PropTypes from 'prop-types';

import Title from 'custom-elements/Title/Title';
import Button from 'custom-elements/Button/Button';

import cardImage from 'assets/images/card.png';

import './DebitCard.scss';

const LIST = [
  { title: '0₽', description: 'бесплатное<br>обслуживание' },
  { title: 'до 4%', description: 'кэшбэк на<br>все покупки' },
  { title: '6 опций', description: 'вознаграждения<br>за покупки' },
  { title: 'до 15%', description: 'кэшбэк от<br>партнеров' },
];

const DebitCard = ({ isImageHide, onClick }) => (
  <section className="debit-card">
    <div className="debit-card__wrapper">
      <div className="debit-card__title">
        <Title title="Дебетовая мультикарта ВТБ" level={1} />
      </div>

      <p className="debit-card__description">
        Получайте вознаграждения от покупок в зависимости от ваших предпочтений
      </p>

      <img
        src={cardImage}
        alt="Дебетовая мультикарта ВТБ"
        className={`debit-card__image ${isImageHide ? 'debit-card__image_hide' : ''}`}
      />

      <ul className="debit-card__list">
        {LIST.map(item => (
          <li key={item.title} className="debit-card__item">
            <h3 className="debit-card__item-title">
              {item.title}
            </h3>

            <p
              dangerouslySetInnerHTML={{ __html: item.description }}
              className="debit-card__item-description"
            />
          </li>
        ))}
      </ul>

      <div className="debit-card__button">
        <Button
          label="Заказать карту"
          color="red"
          onClick={onClick}
        />
      </div>
    </div>
  </section>
);

DebitCard.propTypes = {
  isImageHide: PropTypes.bool,
  onClick: PropTypes.func,
};

DebitCard.defaultProps = {
  isImageHide: false,
  onClick: () => {},
};

export default DebitCard;
