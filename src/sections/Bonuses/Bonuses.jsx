import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { throttle } from 'lodash';

import BonusesButton from 'components/BonusesButton/BonusesButton';
import BonusesOption from 'components/BonusesOption/BonusesOption';

import Title from 'custom-elements/Title/Title';

import { ReactComponent as AutoIcon } from 'assets/images/bonuses/auto.svg';
import { ReactComponent as BorrowerIcon } from 'assets/images/bonuses/borrower.svg';
import { ReactComponent as CollectionIcon } from 'assets/images/bonuses/collection.svg';
import { ReactComponent as JourneysIcon } from 'assets/images/bonuses/journeys.svg';
import { ReactComponent as RestaurantsIcon } from 'assets/images/bonuses/restaurants.svg';
import { ReactComponent as SavingsIcon } from 'assets/images/bonuses/savings.svg';

import './Bonuses.scss';

const ITEMS = [{
  label: 'Заёмщик',
  value: 'borrower',
  description: 'до 10%',
  icon: BorrowerIcon,
}, {
  label: 'Cash Back и рестораны',
  value: 'restaurants',
  description: 'до 4%',
  icon: RestaurantsIcon,
  disabled: true,
}, {
  label: 'Cash Back и Авто',
  value: 'auto',
  description: 'до 4%',
  icon: AutoIcon,
  disabled: true,
}, {
  label: 'Путешествия',
  value: 'journeys',
  description: 'до 3%',
  icon: JourneysIcon,
  disabled: true,
}, {
  label: 'Коллекция',
  value: 'collection',
  description: 'до 3%',
  icon: CollectionIcon,
  disabled: true,
}, {
  label: 'Сбережения',
  value: 'savings',
  description: 'до 1,5%',
  icon: SavingsIcon,
  disabled: true,
}];

const OPTIONS = {
  borrower: [{
    title: 'Опция «Заёмщик»',
    description: 'Расплачивайтесь Мультикартой ВТБ и получайте скидку к ставке по вашему кредиту наличными или ипотеке',
    extendedLink: '',
    tabs: [{
      label: 'Кредит наличными',
      value: 'cash',
    }, {
      label: 'Ипотека',
      value: 'mortgage',
      disabled: true,
    }, {
      label: 'Кредитная карта',
      value: 'card',
      disabled: true,
    }],
    tabsFields: [{
      section: 'cash',
      inputs: [{
        id: 'rate',
        placeholder: 'Ставка по кредиту (%)',
        defaultValue: '13,6',
        maskType: 'number',
      }, {
        id: 'payment',
        placeholder: 'Ежемесячный платеж (₽)',
        defaultValue: '22000',
        maskType: 'number',
      }, {
        id: 'period',
        placeholder: 'Срок кредита (мес)',
        defaultValue: '24',
        caption: 'от 6 месяцев до 7 лет',
        maskType: 'number',
      }, {
        id: 'purchases',
        placeholder: 'Ваши покупки по карте (₽)',
        defaultValue: '75000',
        caption: 'За счёт собственных средств',
        maskType: 'number',
      }],
    }],
  }],
};

const LAPTOP = 1024;

const Bonuses = ({ onOrderButtonClick }) => {
  const [activeButtonValue, setActiveButtonValue] = useState('');
  const [windowWidth, setWindowWidth] = useState(0);

  const handleClick = value => {
    if (windowWidth < LAPTOP) {
      document.body.style.overflowY = 'hidden';
    }
    setActiveButtonValue(value);
  };

  const handleBreadcrumbClick = () => {
    setActiveButtonValue('');
    document.body.style.overflowY = 'auto';
  };

  const handleOrderClick = () => {
    if (windowWidth >= LAPTOP) return;
    handleBreadcrumbClick();
    onOrderButtonClick();
  };

  // TODO заменить на match-media-breakpoint
  const handleResize = throttle(() => {
    const width = window.innerWidth;
    setWindowWidth(width);

    if (width >= LAPTOP) setActiveButtonValue(ITEMS[0].value);
  }, 100);

  useEffect(() => {
    handleResize();

    window.addEventListener('resize', handleResize);
  });

  return (
    <section className="bonuses">
      <div className="bonuses__wrapper">
        <div className="bonuses__title">
          <Title title="Бонусные опции" color="white" />
        </div>

        <p className="bonuses__description">
          Получайте вознаграждения в зависимости от ваших предпочтений
        </p>

        <div className="bonuses__container">
          <div className="bonuses__buttons-container">
            {ITEMS.map(item => (
              <BonusesButton
                key={item.label}
                label={item.label}
                value={item.value}
                description={item.description}
                activeValue={activeButtonValue}
                disabled={item.disabled}
                onClick={handleClick}
              >
                <item.icon />
              </BonusesButton>
            ))}
          </div>

          {OPTIONS[activeButtonValue]?.map(option => (
            windowWidth < LAPTOP
              ? (
                <div key={activeButtonValue} className="bonuses__options-modal">
                  <div className="bonuses__options-breadcrumb-container">
                    <button
                      type="button"
                      className="bonuses__options-breadcrumb-button"
                      onClick={handleBreadcrumbClick}
                    >
                      Вернуться
                    </button>
                  </div>

                  <BonusesOption
                    title={option.title}
                    description={option.description}
                    extendedLink={option.extendedLink}
                    tabs={option.tabs}
                    tabsFields={option.tabsFields}
                    onClick={handleOrderClick}
                  />
                </div>
              ) : (
                <div key={activeButtonValue} className="bonuses__options-container">
                  <BonusesOption
                    title={option.title}
                    description={option.description}
                    extendedLink={option.extendedLink}
                    tabs={option.tabs}
                    tabsFields={option.tabsFields}
                    onClick={onOrderButtonClick}
                  />
                </div>
              )
          ))}
        </div>
      </div>
    </section>
  );
};

Bonuses.propTypes = {
  onOrderButtonClick: PropTypes.func,
};

Bonuses.defaultProps = {
  onOrderButtonClick: () => {},
};

export default Bonuses;
