import React, { useState } from 'react';
import animateScrollTo from 'animated-scroll-to';

import Title from 'custom-elements/Title/Title';

import FaqNavigation from 'components/FaqNavigation/FaqNavigation';
import FaqItem from 'components/FaqItem/FaqItem';
import FaqHowToUse from 'components/FaqHowToUse/FaqHowToUse';

import './Faq.scss';

const TABS = [{
  label: 'Как пользоваться картой',
  value: 'howToUse',
}, {
  label: 'Как начисляется Cash Back',
  value: 'cashBack',
}, {
  label: 'Как получить больше вознаграждений',
  value: 'moreAward',
}, {
  label: 'Мне выдали зарплатную карту на работе, какие у меня преимущества?',
  value: 'salaryCardAdvantages',
}, {
  label: 'Как пополнять карту',
  value: 'howToTopUp',
  disabled: true,
}, {
  label: 'Как сменить PIN-код',
  value: 'howChangePin',
  disabled: true,
}, {
  label: 'Что делать, если карту заблокировали',
  value: 'cardBlocked',
  disabled: true,
}, {
  label: 'Как проверить баланс карты',
  value: 'howCheckBalance',
  disabled: true,
}, {
  label: 'Как оплачивать покупки телефоном',
  value: 'HowPayWithPhone',
  disabled: true,
}];

const ITEMS = [{
  label: 'Как начисляется Cash Back',
  value: 'cashBack',
  description: 'За оплату товаров и услуг картой. За снятие наличных, переводы и платежи в ВТБ-Онлайн, а также некоторые другие операции, cash back не начисляется.',
  extendedLink: 'https://www.vtb.ru/personal/karty/informacija-dlja-derzhatelej-kart/bonusnye-opcii/option_cashback/',
}, {
  label: 'Как получить больше вознаграждений',
  value: 'moreAward',
  description: 'Оформите дополнительные карты членам семьи. Покупки по всем картам суммируются, поэтому процент вознаграждения будет больше. Бесплатно можно оформить до 5 карт.',
  extendedLink: 'https://www.vtb.ru/personal/karty/informacija-dlja-derzhatelej-kart/bonusnye-opcii-old/',
}, {
  label: 'Мне выдали зарплатную карту на работе, какие у меня преимущества?',
  value: 'salaryCardAdvantages',
  description: 'Вы не платите за обслуживание карты и снятие наличных, в любых банкоматах, даже за границей. Независимо от суммы покупок по карте. Кроме того, вы можете на специальных условиях оформить кредит, ипотеку или кредитную карту в нашем банке. Вам не потребуется подтверждать доход и занятость, т.к. вся информация у нас уже есть.',
}];

// TODO сделать переключение активного таба по позиции элемента
export default () => {
  const [activeTab, setActiveTab] = useState('howToUse');

  const handleTabClick = value => {
    setActiveTab(value);
    const selectedElement = document.querySelector(`.faq-item_${value}`);

    if (!selectedElement) return;
    animateScrollTo(selectedElement, { verticalOffset: -100 });
  };

  const handleButtonClick = () => {
    window.open('https://acquiring.vtb.ru/support/faq/', '_blank');
  };

  return (
    <section className="faq">
      <div className="faq__title">
        <Title title="Частые вопросы" />
      </div>

      <div className="faq__container">
        <div className="faq__navigation">
          <FaqNavigation
            activeTab={activeTab}
            items={TABS}
            onTabClick={handleTabClick}
            onButtonClick={handleButtonClick}
          />
        </div>

        <ul className="faq__list">
          <li className="faq-item faq-item_howToUse">
            <FaqHowToUse />
          </li>

          {ITEMS.map(item => (
            <FaqItem
              key={item.label}
              label={item.label}
              value={item.value}
              description={item.description}
              extendedLink={item.extendedLink}
            />
          ))}
        </ul>
      </div>
    </section>
  );
};
