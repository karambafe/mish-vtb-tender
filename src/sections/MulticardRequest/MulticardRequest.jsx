import React from 'react';

import MulticardRequestInfo from 'components/MulticardRequestInfo/MulticardRequestInfo';
import MulticardRequestForm from 'components/Form/Form';

import Title from 'custom-elements/Title/Title';

import './MulticardRequest.scss';

const MulticardRequest = () => (
  <div className="multicard-request">
    <div className="multicard-request__wrapper">
      <div className="multicard-request__title">
        <Title title="Заявка на дебетовую Мультикарту" />
      </div>

      <p className="multicard-request__description">Без комиссии за обслуживание карты  в первый месяц даже при нулевых тратах</p>

      <div className="multicard-request__container">
        <div className="multicard-request__info">
          <MulticardRequestInfo />
        </div>

        <div className="multicard-request__form"><MulticardRequestForm /></div>
      </div>
    </div>
  </div>
);

export default MulticardRequest;
