import React from 'react';

import CardAdvantagesItem from 'components/CardAdvantagesItem/CardAdantagesItem';

import Title from 'custom-elements/Title/Title';

import { ReactComponent as CashWithdrawalIcon } from 'assets/images/card-advantages/cash-withdrawal.svg';
import { ReactComponent as ExtraCardIcon } from 'assets/images/card-advantages/extra-card.svg';
import { ReactComponent as FreeReplenishmentIcon } from 'assets/images/card-advantages/free-replenishment.svg';
import { ReactComponent as FreeServiceIcon } from 'assets/images/card-advantages/free-service.svg';
import { ReactComponent as PushNoticeIcon } from 'assets/images/card-advantages/push-notice.svg';

import './CardAdvantages.scss';

const ITEMS = [
  {
    mainTitle: 'от 5 000 ₽',
    mainDescription: 'сумма покупок по карте в месяц',
    secondaryTitle: '0 ₽',
    secondaryDescription: 'сумма за обслуживание карты в месяц',
    iconDescription: 'Бесплатное обслуживание',
    icon: FreeServiceIcon,
  },
  {
    mainTitle: 'Пополнять карту',
    mainDescription: 'с карт других банков через ВТБ-Онлайн',
    secondaryTitle: '0 ₽',
    secondaryDescription: 'комиссия',
    iconDescription: 'Бесплатное пополнение карты',
    icon: FreeReplenishmentIcon,
  },
  {
    mainTitle: 'Push-уведомления',
    mainDescription: 'по всем операциям',
    secondaryTitle: '0 ₽',
    secondaryDescription: 'стоимость',
    iconDescription: 'Push-уведомления',
    icon: PushNoticeIcon,
  },
  {
    mainTitle: 'до 5 карт',
    mainDescription: 'включая валютные',
    secondaryTitle: '0 ₽',
    secondaryDescription: 'стоимость',
    iconDescription: 'Дополнительные карты',
    icon: ExtraCardIcon,
  },
  {
    mainTitle: 'от 75 000 ₽',
    mainDescription: 'сумма покупок по карте в месяц',
    secondaryTitle: '0 ₽',
    secondaryDescription: 'комиссия',
    iconDescription: 'Снятие наличных в любых банкоматах',
    icon: CashWithdrawalIcon,
  },
];

export default () => (
  <section className="card-advantages">
    <div className="card-advantages__title">
      <Title title="Преимущества карты" />
    </div>

    <ul className="card-advantages__list">
      {ITEMS.map(item => (
        <CardAdvantagesItem
          key={item.mainTitle}
          mainTitle={item.mainTitle}
          mainDescription={item.mainDescription}
          secondaryTitle={item.secondaryTitle}
          secondaryDescription={item.secondaryDescription}
          iconDescription={item.iconDescription}
        >
          <item.icon />
        </CardAdvantagesItem>
      ))}
    </ul>
  </section>
);
