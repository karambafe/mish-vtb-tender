import React from 'react';

import PocketBankItem from 'components/PocketBankItem/PocketBankItem';

import Title from 'custom-elements/Title/Title';

import { ReactComponent as PushIcon } from 'assets/images/pocket-bank/push.svg';
import { ReactComponent as DocumentIcon } from 'assets/images/pocket-bank/document.svg';
import { ReactComponent as NfcIcon } from 'assets/images/pocket-bank/nfc.svg';
import { ReactComponent as AppStoreIcon } from 'assets/images/pocket-bank/app-store.svg';
import { ReactComponent as GooglePlayIcon } from 'assets/images/pocket-bank/google-play.svg';

import phone from 'assets/images/pocket-bank/phone-1x.png';
import phoneRetina from 'assets/images/pocket-bank/phone-2x.png';

import './PocketBank.scss';

const ITEMS = [
  {
    icon: PushIcon,
    title: 'Отслеживайте операции по карте',
    description: 'Подключите бесплатные push-уведомления',
  },
  {
    icon: DocumentIcon,
    title: 'Заказывайте справки в ВТБ-Онлайн',
    description: 'Все справки оформляются на бланке с подписью и печатью',
  },
  {
    icon: NfcIcon,
    title: 'Платите в одно касание',
    description: 'С помощью Apple Pay, Google Pay, Samsung Pay или MIR Pay',
  },
];

export default () => (
  <section className="pocket-bank">
    <img
      src={phone}
      srcSet={`${phoneRetina} 2x`}
      className="pocket-bank__image"
      alt="Iphone"
    />

    <div className="pocket-bank__container">
      <div className="pocket-bank__title">
        <Title title="Банк в кармане" />
      </div>

      <p className="pocket-bank__description">
        Получайте дополнительные преимущества в мобильном приложении ВТБ-Онлайн
      </p>

      <ul className="pocket-bank__list">
        {ITEMS.map(item => (
          <PocketBankItem
            key={item.title}
            title={item.title}
            description={item.description}
          >
            <item.icon />
          </PocketBankItem>
        ))}
      </ul>

      <div className="pocket-bank__mobile-apps">
        <a
          href="https://apps.apple.com/app/apple-store/id472951966"
          target="_blank"
          rel="noopener noreferrer"
          className="pocket-bank__mobile-app"
          aria-label="ссылка на App Store"
        >
          <AppStoreIcon />
        </a>

        <a
          href="https://play.google.com/store/apps/details?id=ru.vtb24.mobilebanking.android&referrer=utm_source%3Dsite%26utm_medium%3Dreferral%26utm_campaign%3Dsite_footer_vtbonline_android_install%26anid%3Dadmob"
          target="_blank"
          rel="noopener noreferrer"
          className="pocket-bank__mobile-app"
          aria-label="ссылка на Google Play"
        >
          <GooglePlayIcon />
        </a>
      </div>
    </div>
  </section>
);
