import React from 'react';

import RatesItem from 'components/RatesItem/RatesItem';

import Title from 'custom-elements/Title/Title';

import { ReactComponent as FileIcon } from 'assets/images/file.svg';

import './Rates.scss';

const ITEMS = [{
  label: 'Валюта',
  isSimple: true,
  list: [{
    description: 'Рубли, Доллары США или Евро',
    isDescriptionBlack: true,
  }],
}, {
  label: 'Обслуживание карты',
  isGray: true,
  list: [{
    title: 'Бесплатно',
    description: 'при сумме покупок по карте от 5 000 ₽ в месяц',
  }, {
    title: '249 ₽ в месяц',
    description: 'если сумма покупок менее 5 000 ₽',
  }, {
    description: 'При оформлении заявки онлайн комиссия за первый месяц не взимается',
    isCaption: true,
  }],
}, {
  label: 'Снятие наличных в любых банкоматах',
  list: [{
    title: 'Бесплатно',
    description: 'в банкоматах ВТБ независимо от суммы покупок',
  }, {
    title: 'Бесплатно',
    description: 'в банкоматах других банков при сумме покупок от 75 000 ₽ в месяц',
  }, {
    title: '350 000 ₽ в день',
    description: 'лимит снятия или 2 000 000 ₽ в месяц',
  }],
}, {
  label: 'Онлайн-платежи и переводы',
  list: [{
    title: 'Бесплатно',
    description: 'при сумме покупок по карте от 75 000 ₽ в месяц',
  }, {
    title: '20 000 ₽ в месяц',
    description: 'лимит бесплатных переводов на карту другого банка',
  }],
}, {
  label: 'Cash back',
  isGray: true,
  list: [{
    description: 'Сash back начисляется бонусными рублями. Вы можете в любой момент перевести cash back на свою карту или использовать его для оплаты покупок, подарков и путешествий в каталоге программы «Мультибонус»',
    isDescriptionBlack: true,
    button: {
      title: 'Перейти в каталог',
      value: 'catalog',
    },
  }],
}, {
  label: 'Пакет оповещений «Карты+»',
  list: [{
    title: 'Бесплатно',
    description: 'при выборе push-уведомлений',
  }, {
    title: '59 ₽ в месяц',
    description: 'при выборе оповещений по СМС',
  }],
}, {
  label: 'Дополнительные карты',
  list: [{
    title: 'Бесплатно',
    description: 'До 5 карт',
  }],
}];

const DOCUMENTS = [{
  title: 'Тарифы по дебетовой и кредитной Мультикарте ВТБ',
  link: '/documents/card-rates.pdf',
}, {
  title: 'Правила комплексного обслуживания физических лиц Банка ВТБ (ПАО)',
  link: '/documents/pko.pdf',
}, {
  title: 'Правила предоставления и использования банковских карт Банка ВТБ (ПАО)',
  link: '/documents/card-rules.pdf',
}, {
  title: 'Правила программ лояльности',
  link: '/documents/loyalty-program.pdf',
}];

export default () => (
  <section className="rates">
    <div className="rates__title">
      <Title title="Тарифы и условия" />
    </div>

    <ul className="rates__list">
      {ITEMS.map(item => (
        <RatesItem
          key={item.label}
          label={item.label}
          list={item.list}
          isGray={item.isGray}
          isSimple={item.isSimple}
        />
      ))}
    </ul>

    <div className="rates__documents-title">
      <Title title="Документы" />
    </div>

    <ul className="rates__list rates__list_documents">
      {DOCUMENTS.map(item => (
        <li key={item.title} className="rates__item">
          <a href={item.link} className="rates__document-link" download>
            <span className="rates__document-icon">
              <FileIcon />
            </span>

            {item.title}
          </a>
        </li>
      ))}
    </ul>

    <div className="rates__caption">
      С полным сборником тарифов по банковским картам можно ознакомиться в разделе&nbsp;
      <a
        href="https://www.vtb.ru/tarify/#tab_0_1#"
        className="rates__caption-link"
        target="_blank"
        rel="noopener noreferrer"
      >
        Тарифы
      </a>
    </div>
  </section>
);
