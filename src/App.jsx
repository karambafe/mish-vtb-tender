import React, { useState, useRef, useEffect } from 'react';
import animateScrollTo from 'animated-scroll-to';
import { throttle } from 'lodash';

import Header from 'components/Header/Header';
import DebitCard from 'sections/DebitCard/DebitCard';
import CardAdvantages from 'sections/CardAdvantages/CardAdvantages';
import Rates from 'sections/Rates/Rates';
import Faq from 'sections/Faq/Faq';
import PoketBank from 'sections/PocketBank/PocketBank';
import Bonuses from 'sections/Bonuses/Bonuses';
import MulticardRequest from 'sections/MulticardRequest/MulticardRequest';
import Footer from 'components/Footer/Footer';

import NavigationBar from 'components/NavigationBar/NavigationBar';

import cardImage from 'assets/images/card.png';
import './App.scss';

const App = () => {
  const [activeTab, setActiveTab] = useState('card');
  const [isRelative, setIsRelative] = useState(false);

  const refMulticardRequest = useRef();
  const refCardSection = useRef();
  const refTabsItems = useRef();

  const handleNavigationBarTabClick = value => {
    setActiveTab(value);
    animateScrollTo(refTabsItems.current, { verticalOffset: -100 });
  };

  const handleOrderButtonClick = () => {
    animateScrollTo(refMulticardRequest.current);
  };

  const handleDebitCardButtonClick = () => {
    animateScrollTo(refMulticardRequest.current);
  };

  const handleScroll = throttle(() => {
    setIsRelative(refCardSection?.current?.getBoundingClientRect()?.top <= 0);
  }, 10);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => window.removeEventListener('scroll', handleScroll);
  });

  return (
    <div className="app">
      <Header />

      <div className="app__wrapper">
        <DebitCard isImageHide={activeTab === 'card'} onClick={handleDebitCardButtonClick} />

        <div className="app__container">
          <NavigationBar
            activeTab={activeTab}
            onTabClick={handleNavigationBarTabClick}
            onButtonClick={handleOrderButtonClick}
          />

          <div ref={refTabsItems}>
            {activeTab === 'card' && (
              <>
                <div
                  ref={refCardSection}
                  className={`app__card-section_${isRelative ? 'relative' : 'static'}`}
                >
                  <CardAdvantages />

                  <div className="app__image-container">
                    <img
                      src={cardImage}
                      alt="Дебетовая мультикарта ВТБ"
                      className="app__image"
                    />
                  </div>
                </div>

                <Bonuses onOrderButtonClick={handleOrderButtonClick} />

                <PoketBank />
              </>
            )}

            {activeTab === 'rates' && <Rates />}
            {activeTab === 'faq' && <Faq />}
          </div>
        </div>
      </div>

      <section ref={refMulticardRequest}>
        <MulticardRequest />
      </section>

      <Footer />
    </div>
  );
};

export default App;
